package first;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class FirstController {

    private final HelloComponent helloComponent;

    @GetMapping("/greetings")
    String greetings(){
        log.info("about to greet");
        return helloComponent.sayHello();
    }

}
