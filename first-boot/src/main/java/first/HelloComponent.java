package first;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HelloComponent {

    @Value("${hello.name:Jude}")
    private String name;

    @PostConstruct
    void postConstruct(){
        log.info("hello component constructed with name param {}", name);
    }

    public String sayHello(){
        return "Hey " + name + "!";
    }

}
