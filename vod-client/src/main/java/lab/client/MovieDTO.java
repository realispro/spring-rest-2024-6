package lab.client;

import lombok.Data;

@Data
public class MovieDTO {

    private int id;
    private String title;
    private String poster;
    private int directorId;
}
