package lab.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

public class VodClient {

    public static void main(String[] args) {

        RestTemplate restTemplate = new RestTemplate();

        if (args.length == 0) {
            printUsage();
            return;
        }

        String command = args[0];

        switch (command) {
            case "GET_MOVIES":

                HttpHeaders headers = new HttpHeaders();
                headers.setBasicAuth("user1", "user1");

                ResponseEntity<MovieDTO[]> responseEntity = restTemplate.exchange(
                        "http://localhost:8080/movies",
                        HttpMethod.GET,
                        new HttpEntity<>(headers),
                        MovieDTO[].class
                );
                System.out.println("response code: " + responseEntity.getStatusCode()
                        + "\n headers: " + responseEntity.getHeaders().toString()
                        + "\n body: " + Arrays.toString(responseEntity.getBody()));
                break;

            case "RATE_MOVIE":
                int movieId = Integer.parseInt(args[1]);
                float rate = Float.parseFloat(args[2]);

                ResponseEntity<RatingDTO> ratingResponseEntity = restTemplate.exchange(
                        "http://localhost:8080/movies/" + movieId + "/ratings",
                        HttpMethod.POST,
                        new HttpEntity<>(RatingDTO.builder().rate(rate).build()),
                        RatingDTO.class
                );

                System.out.println("response code: " + ratingResponseEntity.getStatusCode()
                        + "\n headers: " + ratingResponseEntity.getHeaders().toString()
                        + "\n body: " + ratingResponseEntity.getBody());

                break;
            default:
                printUsage();
        }
    }


    private static void printUsage(){
        System.out.println("usage: java lab.client.VodClient <COMMAND> <ARG>");
        System.out.println("command: GET_MOVIES arg: [], example: java lab.client.VodClient GET_MOVIES");
        System.out.println("command: RATE_MOVIE arg: [movie_id, rate], example: java lab.client.VodClient RATE_MOVIE 1 9");
    }
}
