package vod;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import vod.model.Cinema;
import vod.service.CinemaService;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
@Order(2)
public class VodCinemaRunner implements CommandLineRunner {

    private final CinemaService cinemaService;

    @Override
    public void run(String... args) throws Exception {

        List<Cinema> cinemas = cinemaService.getAllCinemas();
        log.info(cinemas.size() + " cinemas found:");
        cinemas.forEach(System.out::println);

    }
}
