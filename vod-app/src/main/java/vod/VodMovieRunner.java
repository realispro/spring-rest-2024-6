package vod;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import vod.model.Movie;
import vod.model.Rating;
import vod.service.MovieService;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
@Order(1)
public class VodMovieRunner implements CommandLineRunner {

    private final MovieService movieService;

    @Override
    public void run(String... args) throws Exception {

        Movie movie = movieService.getMovieById(2);
        List<Rating> ratings = movieService.getRatingsByMovie(movie);
        log.info("Rating of movie {}", movie.getTitle());
        ratings.forEach(rating -> log.info("rate: {}", rating.getRate()));

    }
}
