package vod.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import vod.web.MovieValidator;
import vod.web.RatingValidator;

@ControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class VodAdvice {

    private final MovieValidator movieValidator;
    private final RatingValidator ratingValidator;

    @InitBinder("movieDTO")
    void initMovieBinder(WebDataBinder binder){
        binder.addValidators(movieValidator);
    }

    @InitBinder("ratingDTO")
    void initRatingBinder(WebDataBinder binder){
        binder.addValidators(ratingValidator);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException e){
        log.error("handling illegal argument exception", e);
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(e.getMessage());
    }

}
