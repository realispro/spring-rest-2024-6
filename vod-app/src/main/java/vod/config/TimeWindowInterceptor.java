package vod.config;

import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.time.LocalTime;

@Component
@Slf4j
public class TimeWindowInterceptor implements HandlerInterceptor {

    @Value("${vod.opening:7}")
    private int opening;
    @Value("${vod.closing:9}")
    private int closing;

    @PostConstruct
    void postConstruct(){
        log.info("time window interceptor activated, opening: {}, closing: {}", opening, closing);
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        int currentHour = LocalTime.now().getHour();
        if(currentHour>=opening && currentHour<closing){
            return true;
        } else {
            response.setStatus(503);
            return false;
        }
    }
}
