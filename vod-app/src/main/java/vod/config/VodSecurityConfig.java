package vod.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class VodSecurityConfig {


    @Bean
    UserDetailsService userDetailsService(DataSource dataSource){

/*        UserDetails user1 = User.withUsername("user1")
                .password("user1")
                .authorities("ROLE_USER")
                .build();

        UserDetails user2 = User.withUsername("user2")
                .password("user2")
                .authorities("ROLE_ADMIN")
                .build();*/

        JdbcUserDetailsManager userDetailsManager = new JdbcUserDetailsManager(dataSource);
        userDetailsManager.setUsersByUsernameQuery("select username, password, 'true' from user where username=?");
        userDetailsManager.setAuthoritiesByUsernameQuery("select username, role from role where username=? ");
        return userDetailsManager;
                //new InMemoryUserDetailsManager(user1, user2);
    }

    @Bean
    PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {

        // diff
        httpSecurity.csrf(csrf->csrf.disable());

        // authorization rules
        httpSecurity.authorizeHttpRequests(registry-> registry
                    .requestMatchers(HttpMethod.GET, "/movies").authenticated()
                    .requestMatchers(HttpMethod.POST, "/movies").hasRole("ADMIN")
                    .anyRequest().permitAll());

        // authentication method
        httpSecurity.httpBasic(Customizer.withDefaults());

        return httpSecurity.build();
    }

}
