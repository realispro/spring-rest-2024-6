package vod.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;
import vod.repository.MovieDao;

@Component
@RequiredArgsConstructor
public class MoviesCountInfoContributor implements InfoContributor {

    private final MovieDao movieDao;

    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("moviesCount", movieDao.findAll().size());
    }
}
