package vod.repository.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import vod.model.Director;
import vod.repository.DirectorDao;

import java.util.List;
import java.util.Optional;

@Repository
@ConditionalOnProperty(value = "vod.dao", havingValue = "jpa")
public abstract class JpaDirectorDao implements DirectorDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Director> findAll() {
        return entityManager.createQuery("select d from Director d").getResultList();
    }

    @Override
    public Optional<Director> findById(Integer id) {
        return Optional.ofNullable(
            entityManager.find(Director.class, id)
        );
    }

    @Override
    public Director save(Director d) {
        entityManager.persist(d);
        return d;
    }
}
