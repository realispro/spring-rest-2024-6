package vod.web;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class RatingValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(RatingController.RatingDTO.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RatingController.RatingDTO dto = (RatingController.RatingDTO) target;
        if(dto.getRate()>10 || dto.getRate()<1){
            errors.rejectValue("rate", "error.rate.out.of.range");
        }
    }
}
