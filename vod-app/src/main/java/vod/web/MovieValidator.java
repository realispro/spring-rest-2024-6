package vod.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import vod.service.MovieService;

@Component
@RequiredArgsConstructor
public class MovieValidator implements Validator {

    private final MovieService movieService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(MovieController.MovieDTO.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MovieController.MovieDTO movieDTO = (MovieController.MovieDTO) target;

        if(movieService.getDirectorById(movieDTO.getDirectorId())==null){
            errors.rejectValue("directorId", "errors.director.missing");
        }
    }
}
