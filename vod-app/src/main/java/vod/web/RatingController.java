package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Movie;
import vod.model.Rating;
import vod.service.MovieService;

import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@Slf4j
public class RatingController {

    private final MovieService movieService;

    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;

    @GetMapping("/movies/{movieId}/ratings")
    ResponseEntity<List<RatingDTO>> getRatingsByMovie(@PathVariable("movieId") int movieId) {
        log.info("about to retrieve ratings for movie {}", movieId);
        Movie movie = movieService.getMovieById(movieId);
        if (movie != null) {
            List<Rating> ratings = movieService.getRatingsByMovie(movie);
            log.info("there is {} ratings for movie {}", ratings.size(), movieId);
            return ResponseEntity.ok(ratings.stream().map(RatingDTO::toDTO).toList());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/movies/{movieId}/ratings")
    ResponseEntity<?> addRating(@PathVariable("movieId") int movieId,
                                @Validated @RequestBody RatingController.RatingDTO ratingDTO, Errors errors, HttpServletRequest request) {
        log.info("about to add new rating to movie {}", movieId);

        if (errors.hasErrors()) {
            Locale locale = localeResolver.resolveLocale(request);
            List<String> errorMessages = errors.getAllErrors().stream()
                    .map(oe -> messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
                    .toList();
            return ResponseEntity.badRequest().body(errorMessages);
        }

        Movie movie = movieService.getMovieById(movieId);

        Rating rateMovie = movieService.rateMovie(movie, ratingDTO.getRate());

        ratingDTO = RatingController.RatingDTO.toDTO(rateMovie);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .build(Map.of("id", ratingDTO.getId()));

        return ResponseEntity.created(uri).body(ratingDTO);
    }


    @Data
    @Builder
    public static class RatingDTO {
        private int id;

        //@Max(10)
        //@Min(1)
        private float rate;

        static RatingDTO toDTO(Rating rating) {
            return RatingDTO.builder()
                    .id(rating.getId())
                    .rate(rating.getRate())
                    .build();
        }

        Rating fromDTO() {
            Rating rating = new Rating();
            rating.setRate(this.rate);
            return rating;
        }
    }
}
