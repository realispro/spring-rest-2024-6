package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.URL;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Cinema;
import vod.model.Director;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@Slf4j
public class MovieController {

    private final MovieService movieService;
    private final CinemaService cinemaService;

    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;


    @GetMapping("/movies") //movies?directorId=1
    List<MovieDTO> getMovies(@RequestParam(value = "directorId", required = false) Integer directorId) {
        log.info("about to retrieve movies. optional director id param: {}", directorId);

        List<Movie> movies;
        if(directorId==null) {
            movies = movieService.getAllMovies();
        } else {
            Director director = movieService.getDirectorById(directorId);
            // TODO handle null director
            movies = movieService.getMoviesByDirector(director);
        }

        log.info("there is {} movies in the list", movies.size());
        return movies.stream()
                .map(MovieDTO::toDTO)
                .toList();
    }

    @GetMapping("/movies/{id}")
    ResponseEntity<MovieDTO> getMovieById(@PathVariable("id") int id) {
        log.info("about to receive movie {}", id);
        Movie movie = movieService.getMovieById(id);

        if (movie != null)
            return ResponseEntity
                    .ok(MovieDTO.toDTO(movie));
        else
            return ResponseEntity
                    .notFound()
                    .build();
    }

    @GetMapping("/directors/{directorId}/movies")
    ResponseEntity<List<MovieDTO>> getMovieByDirectorId(@PathVariable("directorId") int directorId) {
        log.info("about to retrieve movies directed by director {}", directorId);

        Director director = movieService.getDirectorById(directorId);

        if (director != null) {
            List<Movie> movies = movieService.getMoviesByDirector(director);
            log.info("there is {} movies directed by {}", movies.size(), directorId);
            return ResponseEntity.ok(movies.stream().map(MovieDTO::toDTO).toList());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/cinemas/{cinemaId}/movies")
    ResponseEntity<List<MovieDTO>> getMovieByCinemaId(@PathVariable("cinemaId") int cinemaId) {
        log.info("about to retrieve movies in cinema {}", cinemaId);

        Cinema cinema = cinemaService.getCinemaById(cinemaId);

        if (cinema != null) {
            List<Movie> movies = cinemaService.getMoviesInCinema(cinema);
            log.info("there is {} movies in cinema {}", movies.size(), cinemaId);
            return ResponseEntity.ok(movies.stream().map(MovieDTO::toDTO).toList());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(@Validated @RequestBody MovieDTO movieDTO, Errors errors, HttpServletRequest request){
        log.info("about to add new movie {}", movieDTO);

        if(errors.hasErrors()){
            Locale locale = localeResolver.resolveLocale(request);
            List<String> errorMessages = errors.getAllErrors().stream()
                    //.map(oe->oe.getCode() + " " + Arrays.toString(oe.getArguments()))
                    .map(oe->messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
                    .toList();
            // messages_fr_CA
            // messages_fr
            // messages_<OS_LANG> en
            // messages


            return ResponseEntity.badRequest().body(errorMessages);
        }

        if(movieDTO.getTitle().equals("Boom")){
            throw new IllegalArgumentException("Boom");
        }

        Director director = movieService.getDirectorById(movieDTO.directorId);
        Movie movie = movieDTO.fromDTO();
        movie.setDirector(director);

        movie = movieService.addMovie(movie);

        movieDTO = MovieDTO.toDTO(movie);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .build(Map.of(
                        "id", movieDTO.id
                ));

        return ResponseEntity.created(uri).body(movieDTO);
    }

    @Data
    @Builder
    public static class MovieDTO {
        private int id;
        @NotEmpty
        private String title;
        @URL
        private String poster;
        @Positive
        private int directorId;

        static MovieDTO toDTO(Movie movie) {
            return MovieDTO.builder()
                    .id(movie.getId())
                    .title(movie.getTitle())
                    .poster(movie.getPoster())
                    .directorId(movie.getDirector().getId())
                    .build();
        }

        Movie fromDTO(){
            Movie movie = new Movie();
            movie.setTitle(this.title);
            movie.setPoster(this.poster);
            movie.setId(this.id);
            return movie;
        }
    }
}