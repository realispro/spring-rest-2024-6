package vod.web;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CinemaController {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping("/cinemas")
    @ResponseStatus(HttpStatus.OK)
    List<CinemaDTO> getCinemas(@RequestHeader(value = "Foo", required = false) String fooHeader,
                              @RequestHeader Map<String, String> headers,
                              @CookieValue(value = "name1", required = false) String cookieName1) {
        log.info("about to retrieve all cinemas");

        log.info("foo header: " + fooHeader);
        headers.entrySet().stream()
                .filter(entry->entry.getKey().startsWith("foo-"))
                .forEach(entry->log.info("foo- param: {}={}", entry.getKey(), entry.getValue()));

        log.info("cookie param: {}", cookieName1);

        List<Cinema> cinemas = cinemaService.getAllCinemas();
        log.info("there is {} cinemas in the list", cinemas.size());
        return cinemas.stream()
                .map(CinemaDTO::toDTO)
                .toList();
    }

    @GetMapping("/cinemas/{id}")
    ResponseEntity<CinemaDTO> getCinemaById(@PathVariable("id") int id) {
        log.info("about to retrieve cinema {}", id);
        Cinema cinema = cinemaService.getCinemaById(id);
        if (cinema != null) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(CinemaDTO.toDTO(cinema));
        } else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }

    @GetMapping("/movies/{movieId}/cinemas")
    ResponseEntity<List<CinemaDTO>> getCinemasByMovie(@PathVariable("movieId") int movieId) {
        log.info("about to retrieve  cinemas by movie");
        Movie movie = movieService.getMovieById(movieId);
        if (movie == null) {
            return ResponseEntity.notFound().build();
        } else {
            List<Cinema> cinemas = cinemaService.getCinemasByMovie(movie);
            log.info("there is {} cinemas showing movie {}", cinemas.size(), movie);
            return ResponseEntity.ok(cinemas.stream().map(CinemaDTO::toDTO).toList());
        }
    }

    @Data
    @Builder
    public static class CinemaDTO {
        private int id;
        private String name;
        private String logo;

        static CinemaDTO toDTO(Cinema cinema) {
            return CinemaDTO.builder()
                    .id(cinema.getId())
                    .name(cinema.getName())
                    .logo(cinema.getLogo())
                    .build();
        }
    }

}
