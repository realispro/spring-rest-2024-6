package vod;

import io.restassured.RestAssured;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VodApplicationTest {

    @LocalServerPort
    private int port;

    @Test
    void contextLoad(){
        int statusCode = RestAssured.get("http://localhost:" + port + "/cinemas")
                .getStatusCode();
        Assertions.assertEquals(HttpStatus.OK.value(), statusCode);
    }
}
