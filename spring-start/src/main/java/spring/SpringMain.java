package spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import spring.config.TravelConfig;
import spring.impl.Bus;
import spring.impl.Hotel;

import java.time.LocalDate;

public class SpringMain {

    public static void main(String[] args) {

        Person kowalski = new Person("Jan", "Kowalski", new Ticket(LocalDate.now().minusDays(1)));

        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(TravelConfig.class);

        Travel travel = context.getBean(Travel.class);

        travel.travel(kowalski);

        context.close();

        System.out.println("done.");
    }
}
