package spring;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import spring.config.Fast;
import spring.config.TravelStartedEvent;

@Component("travel")
public class Travel {

    private final String name;
    private final Transportation transportation;
    private final Accomodation accomodation;
    private final ApplicationEventPublisher applicationEventPublisher;

    public Travel(@Fast Transportation transportation, Accomodation accomodation, String name, ApplicationEventPublisher applicationEventPublisher) {
        this.transportation = transportation;
        this.accomodation = accomodation;
        this.name = name;
        this.applicationEventPublisher = applicationEventPublisher;
        System.out.println("constructing trip with parametrized constructor...");
    }

    public void travel(Person p){
        System.out.println("started travel " + name + " for a person " + p);
        applicationEventPublisher.publishEvent(new TravelStartedEvent(this, p));
        transportation.transport(p);
        accomodation.host(p);
        transportation.transport(p);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Travel{" +
                "name='" + name + '\'' +
                ", transportation=" + transportation +
                ", accomodation=" + accomodation +
                '}';
    }
}
