package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ComponentScan("spring")
public class TravelConfig {

    @Bean
    String travelName(){
        return "Business travel 2024";
    }

    @Bean
    List<String> meals(){
        return List.of("ramen", "sushi", "sake");
    }
}
