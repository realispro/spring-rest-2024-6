package spring.config;

import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;
import spring.Person;

@Getter
@ToString
public class TravelStartedEvent extends ApplicationEvent {

    private final Person person;

    public TravelStartedEvent(Object source, Person person) {
        super(source);
        this.person = person;
    }
}
