package spring.config;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class TravelStartedEventListener implements ApplicationListener<TravelStartedEvent> {
    @Override
    public void onApplicationEvent(TravelStartedEvent event) {
        System.out.println("Travel started for a person " + event.getPerson());
    }
}
