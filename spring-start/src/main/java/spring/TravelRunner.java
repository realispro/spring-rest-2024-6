package spring;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
@RequiredArgsConstructor
public class TravelRunner implements CommandLineRunner {

    private final Travel travel;

    @Override
    public void run(String... args) throws Exception {
        Person kowalski = new Person("Jan", "Kowalski", new Ticket(LocalDate.now().minusDays(1)));
        travel.travel(kowalski);

    }
}
